using UnityEngine;

public class Pickup : CharacterCore
{
  public NetworkTopology.Powerup pickupType;
  public float bobRate = 1f;
  public float bobHeight = 1f;
  public float lifetime = 0f;
  private float bobTime;
  private float basePos;

  public override void Start() {
    base.Start();
    basePos = transform.position.y;
    bobTime = Random.Range(0, Mathf.PI);
  }

	void Update() {
    bobTime = (bobTime + Time.deltaTime) % (Mathf.PI * 2);
    Vector3 pos = transform.position;
    pos.y = basePos + bobHeight * Mathf.Sin(bobRate * bobTime);
    transform.position = pos;
    if (lifetime > 0) {
      lifetime -= Time.deltaTime;
      if (lifetime <= 0) {
        UnityEngine.Object.Destroy(gameObject);
      } else if (lifetime < 2) {
        GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", -1f + lifetime / 2f);
        GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 1f - lifetime / 2f);
      }
    }
	}

	public virtual void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player") {
      tileMap.server.pickups.Remove(this);
      Kill(true);
      coll.gameObject.GetComponent<PlayerController>().AddPickup(pickupType);
		}
	}
}

