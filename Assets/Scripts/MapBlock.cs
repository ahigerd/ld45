using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class MapBlock {
  public const int BLOCK_SIZE = 16;
  public enum Direction {
    UP,
    RIGHT,
    DOWN,
    LEFT
  };
  public class Exit {
    public Direction dir;
    public NetworkTopology.Powerup powerupRequired;
  };

  public class Placement {
    public MapBlock block;
    public NetworkTopology.Powerup item;
    public NetworkTopology.Powerup powerupRequired;
    public byte[,] tiles {
      get { return block.tiles; }
    }
    public bool hasFly {
      get { return block.hasFly; }
    }
    public bool hasCharge {
      get { return block.hasCharge; }
    }
    public bool hasFreeze {
      get { return block.hasFreeze; }
    }
    public Vector2 itemPos {
      get { return block.itemPos; }
    }
    public Vector2 respawnPos {
      get { return block.respawnPos; }
    }
  };

  public string name;
  public List<Exit> exits;
  public NetworkTopology.Powerup powerup = NetworkTopology.Powerup.None;
  public byte[,] tiles = new byte[BLOCK_SIZE, BLOCK_SIZE];
  public byte[,] overlay = new byte[BLOCK_SIZE, BLOCK_SIZE];
  public byte[,] background = new byte[BLOCK_SIZE, BLOCK_SIZE];
  public bool hasFly = false, hasCharge = false, hasFreeze = false;
  public Vector2 itemPos, respawnPos;

  public static List<MapBlock> blocks = new List<MapBlock>();

  public static void LoadAllBlocks() {
    Debug.Log("LoadAllBlocks");
    blocks.Clear();
    TextAsset[] resources = Resources.LoadAll<TextAsset>("Maps").Cast<TextAsset>().ToArray();
    foreach (TextAsset asset in resources) {
      if (asset.name == "template") continue;
      MapBlock block = LoadFromResource(asset);
      blocks.Add(block);
    }
  }

  public static MapBlock FindBlock(int bits, NetworkTopology.Powerup guardedBy = NetworkTopology.Powerup.Pending) {
    // TODO: bits isn't going to work, need to know more
    List<MapBlock> candidates = new List<MapBlock>();
    foreach (MapBlock block in blocks) {
      if (((bits & 0b1000) == 0b1000) == (block.ExitType(Direction.UP) == NetworkTopology.Powerup.Pending)) continue;
      if (((bits & 0b0100) == 0b0100) == (block.ExitType(Direction.LEFT) == NetworkTopology.Powerup.Pending)) continue;
      if (((bits & 0b0010) == 0b0010) == (block.ExitType(Direction.RIGHT) == NetworkTopology.Powerup.Pending)) continue;
      if (((bits & 0b0001) == 0b0001) == (block.ExitType(Direction.DOWN) == NetworkTopology.Powerup.Pending)) continue;
      if (guardedBy == NetworkTopology.Powerup.Charger && !block.hasCharge) continue;
      if (guardedBy == NetworkTopology.Powerup.Booster && !block.hasFly) continue;
      if (guardedBy == NetworkTopology.Powerup.Quarantine && !block.hasFreeze) continue;
      candidates.Add(block);
    }
    if (candidates.Count == 0) {
      return null;
    }
    return candidates[UnityEngine.Random.Range(0, candidates.Count)];
  }

  public static MapBlock LoadFromResource(TextAsset asset) {
    string[] lines = asset.text.Replace("\r", "").Trim().Split(new Char[]{ '\n' });
    bool inHeader = true;
    string solidTiles = "";
    string openTiles = "";
    MapBlock room = new MapBlock();
    room.name = asset.name;
    room.exits = new List<MapBlock.Exit>();
    int y = 0;
    for (int i = 0; i < lines.Length; i++) {
      string line = lines[i];
      if (inHeader) {
        if (String.IsNullOrWhiteSpace(line)) {
          inHeader = false;
          continue;
        }
        if (line.Contains("=")) {
          string[] kv = line.Split(new Char[]{ '=' }, 2);
          string value = kv[1];
          switch (kv[0]) {
            case "solid":
              solidTiles = value;
              break;
            case "open":
              openTiles = value;
              break;
            case "exit":
              string[] sections = value.Split(new Char[]{ ',' }, 2);
              MapBlock.Exit ex = new MapBlock.Exit();
              ex.dir = (MapBlock.Direction)(int.Parse(sections[0]));
              ex.powerupRequired = (NetworkTopology.Powerup)(int.Parse(sections[1]));
              room.exits.Add(ex);
              break;
          };
        }
      } else {
        for (int x = 0; x < BLOCK_SIZE; x++) {
          int tileType = solidTiles.IndexOf(line[x]);
          if (tileType < 0) {
            tileType = openTiles.IndexOf(line[x]);
            if (tileType == 1) {
              room.respawnPos = new Vector2(x + .5f, y + 1f);
            } else if (tileType == 2) {
              room.itemPos = new Vector2(x + 1f, y);
            } else if (tileType < 0) {
              tileType = 0;
            }
          } else {
            tileType += 65;
          }
          if (tileType == 3 || tileType == 68) {
            room.hasCharge = true;
          } else if (tileType == 4 || tileType == 69) {
            room.hasFreeze = true;
          } else if (tileType == 5 || tileType == 70) {
            room.hasFly = true;
          }
          room.tiles[x, y] = (byte)tileType;
        }
        y++;
      }
    }
    return room;
  }

  public NetworkTopology.Powerup ExitType(Direction dir) {
    foreach (Exit e in exits) {
      if (e.dir == dir) {
        return e.powerupRequired;
      }
    }
    return NetworkTopology.Powerup.Pending;
  }
}
