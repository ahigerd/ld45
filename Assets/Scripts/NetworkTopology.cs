using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NetworkTopology : LevelAwareBehaviour {
  public enum Powerup {
    None,
    Charger,
    Booster,
    Quarantine,
    SignalAmplifier,
    Melee,
    Ranged,
    Armor,
    Health,
    Warp,
    Recovery,
    Pending,
  };

  public class Warp {
    public static float[] warpColors = new float[]{
      120f / 360f,
      0f,
      240f / 360f,
      300f / 360f,
      180f / 360f,
      30f / 360f,
      270f / 360f,
      210f / 360f,
    };
    public Powerup powerupRequired;
    public Server source;
    public Vector2Int sourceBlock;
    public Transform sourceTransform;
    public Server target;
    public Vector2Int targetBlock;
    public Transform targetTransform;

    public Warp(Transform prefab) {
      sourceTransform = UnityEngine.Object.Instantiate(prefab);
      sourceTransform.GetComponent<WarpBehaviour>().warp = this;
      targetTransform = UnityEngine.Object.Instantiate(prefab);
      targetTransform.GetComponent<WarpBehaviour>().warp = this;
    }
  }

  public class Server {
    public const int BLOCKS_X = TileMap.MAP_WIDTH / MapBlock.BLOCK_SIZE;
    public const int BLOCKS_Y = TileMap.MAP_HEIGHT / MapBlock.BLOCK_SIZE;
    public NetworkTopology net;
    public int serverID;
    public List<Warp> warps = new List<Warp>();
    public List<Powerup> powerups = new List<Powerup>();
    public MapBlock.Placement[,] blocks = new MapBlock.Placement[BLOCKS_X, BLOCKS_Y];
    public TileMap tilemap;
    public List<Pickup> pickups = new List<Pickup>();
    public List<EnemyCore> enemies = new List<EnemyCore>();

    public void AddWarp(Transform warpPrefab, Server target, Powerup powerup = Powerup.Pending) {
      Warp warp = new Warp(warpPrefab);

      warp.source = this;
      var sourceMain = warp.sourceTransform.GetComponent<ParticleSystem>().main;
      var startColor = sourceMain.startColor;
      startColor.color = Color.HSVToRGB(Warp.warpColors[target.serverID], 1f, 1f);
      sourceMain.startColor = startColor;

      warp.target = target;
      var targetMain = warp.targetTransform.GetComponent<ParticleSystem>().main;
      startColor.color = Color.HSVToRGB(Warp.warpColors[this.serverID], 1f, 1f);
      targetMain.startColor = startColor;

      warp.powerupRequired = powerup;
      warps.Add(warp);
      target.warps.Add(warp);
    }

    public void WillDestroy(CharacterCore obj) {
      if (obj is Pickup) {
        pickups.Remove(obj as Pickup);
      }
      if (obj is EnemyCore) {
        enemies.Remove(obj as EnemyCore);
      }
    }

    public void SetCurrent(bool on) {
      if (on) {
        Camera.main.backgroundColor = Color.HSVToRGB(Warp.warpColors[serverID], .6f, .5f);
      }
      tilemap.IsVisible = on;
      foreach (Warp warp in warps) {
        if (warp.source == this) {
          warp.sourceTransform.gameObject.SetActive(on);
        } else if (warp.target == this) {
          warp.targetTransform.gameObject.SetActive(on);
        }
      }
      foreach (Pickup t in pickups) {
        t.gameObject.SetActive(on);
      }
      foreach (EnemyCore t in enemies) {
        t.gameObject.SetActive(on);
      }
    }

    public void BuildRooms() {
      int[,] bits = new int[MapBlock.BLOCK_SIZE, MapBlock.BLOCK_SIZE];
      for (int y = 0; y < BLOCKS_Y; y++) {
        for (int x = 0; x < BLOCKS_X; x++) {
          blocks[x, y] = new MapBlock.Placement();
        }
      }

      // First place the points of interest
      foreach (Warp warp in warps) {
        int x, y;
        do {
          x = UnityEngine.Random.Range(0, BLOCKS_X);
          y = UnityEngine.Random.Range(0, BLOCKS_Y);
        } while ((bits[x, y] & 0b10000) != 0);
        bits[x, y] |= 0b10000;
        if (warp.source == this) {
          warp.sourceBlock = new Vector2Int(x, y);
        } else {
          warp.targetBlock = new Vector2Int(x, y);
        }
      }
      foreach (Powerup powerup in powerups) {
        int x, y;
        do {
          x = UnityEngine.Random.Range(0, BLOCKS_X);
          y = UnityEngine.Random.Range(0, BLOCKS_Y);
        } while ((bits[x, y] & 0b10000) != 0);
        bits[x, y] |= 0b10000;
        blocks[x, y].item = powerup;
        float rng = UnityEngine.Random.value;
        blocks[x, y].powerupRequired = Powerup.None;
        switch (powerup) {
          case Powerup.None:
          case Powerup.Melee:
          case Powerup.Booster:
          case Powerup.Warp:
            // never anything
            break;
          case Powerup.Charger:
            // could optionally require flight, or not
            if (rng < .4f) {
              blocks[x, y].powerupRequired = Powerup.Booster;
            }
            break;
          case Powerup.Ranged:
          case Powerup.Quarantine:
            // charger or flight OK
            if (rng < .4f) {
              blocks[x, y].powerupRequired = Powerup.Charger;
            } else if (rng < .8f) {
              blocks[x, y].powerupRequired = Powerup.Booster;
            }
            break;
          case Powerup.SignalAmplifier:
            // quarantine only
            blocks[x, y].powerupRequired = Powerup.Quarantine;
            break;
          default:
            // all optional powerups can be guarded by anything
            if (rng < .25f) {
              blocks[x, y].powerupRequired = Powerup.Charger;
            } else if (rng < .5f) {
              blocks[x, y].powerupRequired = Powerup.Booster;
            } else if (rng < .75f) {
              blocks[x, y].powerupRequired = Powerup.Quarantine;
            }
            break;
        }
      }
      // Then fill in some initial routing options
      for (int y = 0; y < MapBlock.BLOCK_SIZE; y++) {
        for (int x = 0; x < MapBlock.BLOCK_SIZE; x++) {
          int oldBits = bits[x, y];
          do {
            bits[x, y] = oldBits;
            if (bits[x, y] != 0) {
              bits[x, y] |= (1 << UnityEngine.Random.Range(0, 4));
            } else {
              bits[x, y] = (1 << UnityEngine.Random.Range(0, 4)) | (1 << UnityEngine.Random.Range(0, 4));
            }
            if (y == 0) {
              bits[x, y] &= 0b0111;
            } else if ((bits[x, y] & 0b1000) > 0 || (bits[x, y - 1] & 0b0001) > 0) {
              bits[x, y] |= 0b1000;
              bits[x, y - 1] |= 0b0001;
            }
            if (x == 0) {
              bits[x, y] &= 0b1011;
            } else if ((bits[x, y] & 0b0100) > 0 || (bits[x - 1, y] & 0b0010) > 0) {
              bits[x, y] |= 0b0100;
              bits[x - 1, y] |= 0b0010;
            }
            if (x == MapBlock.BLOCK_SIZE - 1) bits[x, y] &= ~0b0010;
            if (y == MapBlock.BLOCK_SIZE - 1) bits[x, y] &= ~0b0001;
          } while ((bits[x, y] & 0b1111) == 0);
        }
      }
      // Then take a fix-up pass to ensure routability
      int reachable = MarkReachable(bits, 0, 0);
      int retry = BLOCKS_X * BLOCKS_Y;
      while (reachable < BLOCKS_X * BLOCKS_Y && retry > 0) {
        --retry;
        // Find an unreachable block and make it reachable
        for (int y = BLOCKS_Y - 1; y >= 0; --y) {
          for (int x = BLOCKS_X - 1; x >= 0; --x) {
            // Reachable, so skip it
            if ((bits[x, y] & 0b10000000) > 0) continue;

            if (x < BLOCKS_X - 1 && (bits[x + 1, y] & 0b10010000) == 0b10000000) {
              // Can find a reachable cell by going right
              bits[x, y] |= 0b0010;
              bits[x + 1, y] |= 0b0100;
            } else if (x > 0 && (bits[x - 1, y] & 0b10010000) == 0b10000000) {
              // Can find a reachable cell by going left
              bits[x, y] |= 0b0100;
              bits[x - 1, y] |= 0b0010;
            } else if (y < BLOCKS_Y - 1 && (bits[x, y + 1] & 0b10010000) == 0b10000000) {
              // Can find a reachable cell by going down
              bits[x, y] |= 0b0001;
              bits[x, y + 1] |= 0b1000;
            } else if (y > 0 && (bits[x, y - 1] & 0b10010000) == 0b10000000) {
              // Can find a reachable cell by going up
              bits[x, y] |= 0b1000;
              bits[x, y - 1] |= 0b0001;
            } else {
              // No adjacent reachable cells
              continue;
            }
            reachable += MarkReachable(bits, x, y);
          }
        }
      }
      // Take a pass to clean up the edges
      for (int y = 0; y < BLOCKS_Y; y++) {
        bits[0, y] &= ~0b0100;
        if ((bits[0, y] & 0b1111) == 0) {
          bits[0, y] |= 0b0010;
          bits[1, y] |= 0b0100;
        }
        bits[BLOCKS_X - 1, y] &= ~0b0010;
        if ((bits[BLOCKS_X - 1, y] & 0b1111) == 0) {
          bits[BLOCKS_X - 1, y] |= 0b0100;
          bits[BLOCKS_X - 2, y] |= 0b0010;
        }
      }
      for (int x = 0; x < BLOCKS_X; x++) {
        bits[x, 0] &= ~0b1000;
        if ((bits[x, 0] & 0b1111) == 0) {
          bits[x, 0] |= 0b0001;
          bits[x, 1] |= 0b1000;
        }
        bits[x, BLOCKS_Y - 1] &= ~0b0001;
        if ((bits[x, BLOCKS_Y - 1] & 0b1111) == 0) {
          bits[x, BLOCKS_Y - 1] |= 0b1000;
          bits[x, BLOCKS_Y - 2] |= 0b0001;
        }
      }
      // Finally, use the metadata to get the maps out of the resources
      for (int y = 0; y < BLOCKS_Y; y++) {
        for (int x = 0; x < BLOCKS_X; x++) {
          blocks[x, y].block = MapBlock.FindBlock(bits[x, y], blocks[x, y].powerupRequired);
          if (blocks[x, y].block == null) {
            Utility.Log("Could not find block for {0} {1} at ({2}, {3})", bits[x, y], blocks[x, y].powerupRequired, x, y);
          } else if (blocks[x, y].item != NetworkTopology.Powerup.None && blocks[x, y].item != NetworkTopology.Powerup.Warp) {
            bool found = false;
            foreach (Pickup pickup in net.pickupPrefabs) {
              if (pickup.pickupType == blocks[x, y].item) {
                float ix = x * MapBlock.BLOCK_SIZE + blocks[x, y].itemPos.x;
                float iy = y * MapBlock.BLOCK_SIZE + blocks[x, y].itemPos.y + 1f;
                pickups.Add(UnityEngine.Object.Instantiate(pickup, new Vector3(ix, -iy, 0), Quaternion.identity, tilemap.transform));
                found = true;
                break;
              }
            }
            if (!found) {
              // make the map generate nicely while implementing
              blocks[x, y].item = NetworkTopology.Powerup.None;
            }
          }
        }
      }
      foreach (Warp warp in warps) {
        Vector2Int blockPos = (warp.source == this) ? warp.sourceBlock : warp.targetBlock;
        MapBlock.Placement block = blocks[blockPos.x, blockPos.y];
        float x = blockPos.x * MapBlock.BLOCK_SIZE + block.block.itemPos.x;
        float y = blockPos.y * MapBlock.BLOCK_SIZE + block.block.itemPos.y;
        Transform transform = (warp.source == this ? warp.sourceTransform : warp.targetTransform);
        transform.parent = tilemap.transform;
        transform.position = new Vector3(x, -y, -1f);
        block.item = NetworkTopology.Powerup.Warp;
        block.powerupRequired = warp.powerupRequired;
      }
      tilemap.mapBlocks = blocks;
    }

    private static int MarkReachable(int[,] bits, int x, int y) {
      if ((bits[x, y] & 0b10000000) > 0) {
        // Don't double-count cells.
        return 0;
      }
      int reachable = 1;
      bits[x, y] |= 0b10000000;
      if (y > 0 && (bits[x, y] & 0b1000) > 0)
        reachable += MarkReachable(bits, x, y - 1);
      if (x < Server.BLOCKS_X - 1 && (bits[x, y] & 0b0100) > 0)
        reachable += MarkReachable(bits, x - 1, y);
      if (x > 0 && (bits[x, y] & 0b0010) > 0)
        reachable += MarkReachable(bits, x + 1, y);
      if (y < Server.BLOCKS_Y - 1 && (bits[x, y] & 0b0001) > 0)
        reachable += MarkReachable(bits, x, y + 1);
      return reachable;
    }
  }

  public Transform warpPrefab;
  public TileMap tilemapPrefab;
  public Pickup recoveryPrefab;
  public List<Pickup> pickupPrefabs;
  public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
  public BossWorm bossPrefab;
  public Server currentServer;
  public Warp lastWarp;

  [HideInInspector]
  public Server root;
  [HideInInspector]
  public List<Server> nodes;
  [HideInInspector]
  public Vector2 spawnPoint = Vector2.zero;

  private Server AddServer(Server origin, Powerup power = Powerup.None) {
    Server target = new Server();
    target.serverID = nodes.Count;
    target.net = this;
    nodes.Add(target);
    origin.AddWarp(warpPrefab, target, power);
    return target;
  }

  private Server PickPowerupNode() {
    while (true) {
      int choice = UnityEngine.Random.Range(2, nodes.Count);
      if (nodes[choice].powerups.Count > 0) continue;
      return nodes[choice];
    }
  }

  public void Awake() {
    MapBlock.LoadAllBlocks();
    nodes = new List<Server>();
    root = new Server();
    root.net = this;
    root.powerups.Add(Powerup.Melee);
    nodes.Add(root);

    Server home = AddServer(root, Powerup.SignalAmplifier);
    Server s1 = AddServer(root, Powerup.None);
    Server s2 = AddServer(root, Powerup.None);
    Server s3 = AddServer(s1);
    Server s4 = AddServer(s1);
    Server s5 = AddServer(s2);
    Server s6 = AddServer(s2);
    s3.AddWarp(warpPrefab, s4);
    s3.AddWarp(warpPrefab, s6);
    s4.AddWarp(warpPrefab, s5);
    s5.AddWarp(warpPrefab, s6);

    currentServer = root;

    foreach (Server server in nodes) {
      server.tilemap = UnityEngine.Object.Instantiate(tilemapPrefab);
      server.tilemap.server = server;
    }
  }

  public override void OnGameStart() {
    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    player.GetComponent<SpriteRenderer>().enabled = true;

    currentServer = root;

    for (int i = nodes.Count - 1; i >= 2; --i) {
      nodes[i].powerups.Clear();
      foreach (Warp warp in nodes[i].warps) {
        if (warp.source == root || warp.target == root) {
          continue;
        }
        warp.powerupRequired = Powerup.Pending;
      }
    }

    // Must put flight in s1 or s2 to avoid making the game unwinnable
    Server flightHost = UnityEngine.Random.value > 0.5 ? nodes[2] : nodes[3];
    flightHost.powerups.Add(Powerup.Booster);

    Server smashHost = PickPowerupNode();
    smashHost.powerups.Add(Powerup.Charger);

    Server freezeHost = PickPowerupNode();
    freezeHost.powerups.Add(Powerup.Quarantine);

    List<Warp> openWarps = new List<Warp>();
    foreach (Server server in nodes) {
      foreach (Warp warp in server.warps) {
        if (warp.powerupRequired == Powerup.Pending) {
          openWarps.Add(warp);
        }
      }
    }

    foreach (Server server in nodes) {
      if (server.powerups.Count < 1) continue;
      int choice;
      do {
        choice = UnityEngine.Random.Range(0, openWarps.Count);
      } while (openWarps[choice].source == server || openWarps[choice].target == server);
      openWarps[choice].powerupRequired = server.powerups[0];
      openWarps.RemoveAt(choice);
    }

    Server rangedHost = PickPowerupNode();
    rangedHost.powerups.Add(Powerup.Ranged);

    Server armorHost = PickPowerupNode();
    armorHost.powerups.Add(Powerup.Armor);

    Server ampHost = PickPowerupNode();
    ampHost.powerups.Add(Powerup.SignalAmplifier);

    foreach (Warp warp in openWarps) {
      warp.powerupRequired = Powerup.None;
    }

    foreach (Server server in nodes) {
      server.powerups.Add(Powerup.Health);
      server.BuildRooms();
      server.tilemap.NewLevel();
      List<CharacterCore> otherEntities = new List<CharacterCore>(server.tilemap.GetComponentsInChildren<CharacterCore>());
      int maxEnemies = 20;
      int minRange = 5;
      if (server.serverID == 0) {
        maxEnemies = 10;
        minRange = 8;
      } else if (server.serverID == 1) {
        maxEnemies = 50;
        minRange = 2;
        BossWorm worm = UnityEngine.Object.Instantiate(bossPrefab, new Vector3(-10, 10, 0), Quaternion.identity).GetComponent<BossWorm>();
        worm._network = this;
        worm._server = server;
        server.enemies.Add(worm);
      }
      for (int i = 0; i < maxEnemies; i++) {
        EnemyCore prefab = enemyPrefabs[UnityEngine.Random.Range(0, enemyPrefabs.Count)];
        Vector2 pos = Vector2.zero;
        bool clear;
        int attempts = 0;
        do {
          pos.x = UnityEngine.Random.Range(0, TileMap.MAP_WIDTH) + .5f;
          pos.y = UnityEngine.Random.Range(0, TileMap.MAP_HEIGHT);
          clear = prefab.CanSpawnAt(server.tilemap, pos.x, pos.y - .05f);
          if (clear) {
            int blockX = (int)(pos.x / MapBlock.BLOCK_SIZE);
            int blockY = (int)(pos.y / MapBlock.BLOCK_SIZE);
            int inBlockX = (int)(pos.x - blockX * MapBlock.BLOCK_SIZE);
            int inBlockY = (int)(pos.y - blockY * MapBlock.BLOCK_SIZE);
            MapBlock.Placement block = server.blocks[blockX, blockY];
            if (block.item == NetworkTopology.Powerup.Warp) {
              clear = (Mathf.Abs(inBlockX - block.itemPos.x) + Mathf.Abs(inBlockY - block.itemPos.y) >= 5) &&
                  (Mathf.Abs(inBlockX - block.respawnPos.x) + Mathf.Abs(inBlockY - block.respawnPos.y) >= 5);
            }
          }
          if (clear) {
            foreach (CharacterCore other in otherEntities) {
              if (Mathf.Abs(pos.x - other.origin.x) + Mathf.Abs(pos.y - other.origin.y) < 3) {
                clear = false;
                break;
              }
            }
          }
          ++attempts;
          if (attempts > 500 && server.serverID > 0) {
            // If we can't figure out how to place the enemy after this long, try a different type
            prefab = enemyPrefabs[UnityEngine.Random.Range(0, enemyPrefabs.Count)];
          }
        } while (!clear && attempts < 1000);
        if (!clear) {
          Utility.Log("!!! couldn't place an enemy {0}", server.enemies.Count);
          break;
        } else {
          EnemyCore enemy = UnityEngine.Object.Instantiate(prefab).GetComponent<EnemyCore>();
          enemy._network = this;
          enemy._server = server;
          enemy.origin = pos;
          server.enemies.Add(enemy);
          otherEntities.Add(enemy);
        }
      }
    }
    lastWarp = root.warps[0];
    OnLevelStart();
  }

  public override void OnLevelStart() {
    foreach (Server server in nodes) {
      server.SetCurrent(server == currentServer);
    }

    Vector2Int startBlockXY = lastWarp.source == currentServer ? lastWarp.sourceBlock : lastWarp.targetBlock;
    MapBlock.Placement startBlock = currentServer.blocks[startBlockXY.x, startBlockXY.y];
    spawnPoint.x = startBlockXY.x * MapBlock.BLOCK_SIZE + startBlock.respawnPos.x;
    spawnPoint.y = startBlockXY.y * MapBlock.BLOCK_SIZE + startBlock.respawnPos.y;

    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    player.respawnPoint = spawnPoint;
    player.origin = spawnPoint;
  }

  public override void OnGameOver() {
    PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    player.GetComponent<SpriteRenderer>().enabled = false;

    foreach (Server server in nodes) {
      server.SetCurrent(false);
    }
  }
}
