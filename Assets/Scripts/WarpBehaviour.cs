using UnityEngine;
using System.Collections.Generic;

public class WarpBehaviour : EnemyCore {
  public NetworkTopology.Warp warp;
  public ParticleSystem tractor;
  public Material crossfadeMaterial;
  private float powerLevel = 0f;
  private bool inContact = false;
  private Transform player;
  private Mesh crossfadeMesh;

  public void Awake() {
    gameObject.SetActive(false);
  }

  public override void Start() {
    base.Start();
    tractor.gameObject.SetActive(false);
    var main = tractor.main;
    var startColor = main.startColor;
    startColor.color = (GetComponent<ParticleSystem>() as ParticleSystem).main.startColor.color;
    main.startColor = startColor;
    (GetComponent<BoxCollider2D>() as BoxCollider2D).offset = Vector2.zero;
		crossfadeMesh = new Mesh();
		crossfadeMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(new Vector3(-1, 1, 0));
		vertices.Add(new Vector3(1, 1, 0));
		vertices.Add(new Vector3(1, -1, 0));
		vertices.Add(new Vector3(-1, -1, 0));
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));
		uv.Add(new Vector2(0, 1));
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);
		crossfadeMesh.vertices = vertices.ToArray();
		crossfadeMesh.uv = uv.ToArray();
		crossfadeMesh.triangles = triangles.ToArray();
  }

  public override void OnCollisionEnter2D(Collision2D coll) {
    if (coll.gameObject.tag == "Player") {
      tractor.gameObject.SetActive(true);
      inContact = true;
      player = coll.gameObject.transform;
    }
  }

  public void OnCollisionExit2D(Collision2D coll) {
    if (coll.gameObject.tag == "Player") {
      tractor.gameObject.SetActive(false);
      inContact = false;
    }
  }

  public void Update() {
    if (inContact || powerLevel > 0) {
      Vector2 vector = this.gameObject.transform.position - (player.position + Vector3.up);
      float angle = Vector3.SignedAngle(vector, Vector3.left, Vector3.back) - 30;
      var shape = tractor.shape;
      shape.radius = Mathf.Max(vector.magnitude, 2f);
      shape.rotation = new Vector3(0, 0, angle);
      var emission = tractor.emission;
      var rateOverTime = emission.rateOverTime;
      if (inContact) {
        float factor = Mathf.Min(1f / Mathf.Min(vector.magnitude, .1f), 5f);
        rateOverTime.constant = factor * 10;
        if (vector.magnitude < 2f) {
          powerLevel += Time.deltaTime * factor;
          if (powerLevel > 10) {
            powerLevel = 0;
            tractor.gameObject.SetActive(false);
            var network = GameObject.FindWithTag("Map").GetComponent<NetworkTopology>();
            if (network.currentServer == warp.source) {
              network.currentServer = warp.target;
            } else {
              network.currentServer = warp.source;
            }
            network.lastWarp = warp;
            GameManager.instance.NewLevel();
            return;
          }
          if (powerLevel > 2) {
            player.GetComponent<PlayerController>().velocity.y = 0;
            player.Translate(Vector3.ClampMagnitude(vector.normalized * .03f * powerLevel, vector.magnitude));
          }
        }
      } else {
        rateOverTime.constant = Mathf.Min(1, rateOverTime.constant - Time.deltaTime * 100);
        powerLevel -= Time.deltaTime * 50f;
        if (powerLevel <= 0) {
          tractor.gameObject.SetActive(false);
        }
      }
    }
    if (powerLevel > 5) {
      Matrix4x4 matrix = Matrix4x4.identity;
      matrix.SetTRS(Camera.main.transform.position + Vector3.forward, Quaternion.identity, new Vector3(Screen.width, Screen.height, 1));
      var network = GameObject.FindWithTag("Map").GetComponent<NetworkTopology>();
      int serverID = (network.currentServer == warp.source) ? warp.target.serverID : warp.source.serverID;
      crossfadeMaterial.color = Color.Lerp(Color.clear, Color.HSVToRGB(NetworkTopology.Warp.warpColors[serverID], .6f, .5f), (powerLevel - 5f) / 5f);
      Graphics.DrawMesh(crossfadeMesh, matrix, crossfadeMaterial, 0, null, 0, null, false, false, false);
    }
  }
}
