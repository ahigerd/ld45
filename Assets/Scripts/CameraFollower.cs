using UnityEngine;

public class CameraFollower : MonoBehaviour {
  public float speed = 1f;
  public int panZone = 0;

  void Update() {
    // TODO: this is temporary
    //transform.Translate(new Vector2(Input.GetAxis("Horizontal") * speed, Input.GetAxis("Vertical") * speed));
  }

  private float lastHeight;
  void LateUpdate() {
    if (!GameManager.IsRunning()) { // || GameManager.inDialog) {
      // No action is occurring
      return;
    }
    if (Screen.height != lastHeight) {
      lastHeight = Screen.height;
      float divisor = 0f;
      float vBlocks;
      do {
        divisor += 1f;
        vBlocks = Mathf.Floor(Screen.height / 32.0f / divisor);
      } while (vBlocks > 12);
      Camera.main.orthographicSize = Screen.height / 32.0f / divisor / 2f;
    }

    float halfHeight = Camera.main.orthographicSize;
    float halfWidth = halfHeight / Screen.height * Screen.width;
    float cameraX = Mathf.Clamp(transform.position.x, halfWidth, TileMap.MAP_WIDTH - halfWidth);
    float cameraY = Mathf.Clamp(-transform.position.y, halfHeight, TileMap.MAP_HEIGHT - halfHeight);

    //cameraX = Mathf.Round(cameraX * 32f) / 32f;
    //cameraY = Mathf.Round(cameraY * 32f) / 32f;
    Camera.main.transform.position = new Vector3(cameraX, -cameraY, -10);
  }
};
