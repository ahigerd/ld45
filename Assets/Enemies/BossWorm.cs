using UnityEngine;
using System.Collections.Generic;

public class BossWorm : EnemyCore {
  public static BossWorm head = null;
  public static bool initialized = false;
  public static bool defeated = false;
  private static BossWorm segmentPrefab = null;
  public int numSegments = 0;
  public SpriteAnimationManager burstPrefab;
  public List<BossWorm> segments = null;
  public BossWorm previous = null;
  public BossWorm next = null;
  public bool isTail = false;

  public Vector2 velocity = Vector2.zero;
  static public PlayerController player = null;
  private int burstCount = 4;

  public void Start() {
    if (segmentPrefab == this) {
      return;
    }
    if (head == null) {
      head = this;
      segmentPrefab = UnityEngine.Object.Instantiate(this);
    }
    base.Start();
  }

  private void InitBody() {
    List<BossWorm> created = new List<BossWorm>();
    created.Add(this);
    for (int i = 1; i < numSegments; i++) {
      BossWorm segment = UnityEngine.Object.Instantiate(segmentPrefab);
      created[created.Count - 1].next = segment;
      segment.previous = created[created.Count - 1];
      created.Add(segment);
      segment.isTail = (i == numSegments - 1);
      segment.GetComponent<SpriteAnimationManager>().SwitchAnimation(i == numSegments - 1 ? "Tail" : "Body", true);
      segment.layer = -2.0f + i * .01f;
    }
    layer = -2f;
    segments = created;
    player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    initialized = true;
    Debug.Log("InitBody done");
  }

  public override void Update() {
    if (defeated) {
      deathTime -= Time.deltaTime;
      if (deathTime <= 0) {
        if (next != null) {
          next.previous = null;
        }
        if (isTail) {
          GameManager.instance.WinGame();
        }
        Kill(true);
      } else if (deathTime < burstCount * .25f) {
        --burstCount;
        Vector2 rand = UnityEngine.Random.insideUnitCircle;
        SpriteAnimationManager sam = UnityEngine.Object.Instantiate(burstPrefab, transform.position + new Vector3(rand.x, rand.y, -2.5f), Quaternion.identity).GetComponent<SpriteAnimationManager>();
        player.audioSource.PlayOneShot(deathSound);
        sam.SwitchAnimation("Idle", true);
        sam.PlayOnce("Burst");
      }
      return;
    }
    base.Update();
    if (!initialized && head == this) {
      InitBody();
    }
    if (previous == null && head != this) {
      return;
    }
    Vector2 target = (previous != null ? previous.origin : player.origin);
    Vector2 dir = (target - origin).normalized;
    velocity = (velocity + dir.normalized * Time.deltaTime * jumpPower).normalized * speed;
    if (Mathf.Abs(velocity.x) > 0.2) {
      GetComponent<SpriteRenderer>().flipX = (velocity.x > 0);
    }
    origin += velocity * Time.deltaTime;
  }

	public override void OnCollisionEnter2D(Collision2D coll) {
    if (this != head) {
      return;
    }
    base.OnCollisionEnter2D(coll);
  }

  public override void OnKilled() {
    defeated = true;
    float dt = 1f;
    foreach (BossWorm s in segments) {
      s.deathTime = dt;
      dt += .5f;
    }
  }
}
