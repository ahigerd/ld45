using UnityEngine;

public class Grounder : EnemyCore {
  private bool moveLeft = false;
  private float yVelocity = 0f;
  private bool jumping = false;

  public override void Start() {
    base.Start();
    moveLeft = (Random.value < 0.5f);
    Flip();
  }

  public override void Update() {
    base.Update();
    if (deathTime > 0) return;
    bool wasGrounded = isGrounded;
    float lookX = origin.x + (moveLeft ? -.5f : .5f), lookY = origin.y + .1f;
    float jumpX = origin.x + (moveLeft ? -.8f : .8f);
    if (!tileMap.IsSolid(lookX, lookY - 1f) && !tileMap.IsSolid(lookX, lookY) && !tileMap.IsSolid(lookX, lookY + 1f)) {
      // Don't want to fall more than one block
      Flip();
    } else if (isGrounded && tileMap.IsSolid(jumpX, lookY - 1) && !tileMap.IsSolid(jumpX, lookY - 2)) {
      // Can jump up
      yVelocity = -jumpPower;
      jumping = true;
      sam.PlayOnce("JumpStart");
    }
    float xVelocity = moveLeft ? -speed : speed;
    yVelocity += Time.deltaTime * GRAVITY;
    if (!jumping && !isGrounded) {
      // Don't keep zooming forward when falling
      xVelocity *= .25f;
    }
    if (isGrounded && blinkTime > 0) {
      xVelocity = 0;
    }
    Vector2 moved = Move(new Vector2(xVelocity, yVelocity));
    if (moved.x == 0 && isGrounded) {
      Flip();
    }
    yVelocity = moved.y;
    if (isGrounded) {
      if (!wasGrounded) {
        sam.PlayOnce("Landing");
      }
      sam.SwitchAnimation("Idle", true);
      jumping = false;
    } else {
      sam.SwitchAnimation("Jump", true);
    }
  }

  private void Flip() {
    moveLeft = !moveLeft;
    GetComponent<SpriteRenderer>().flipX = !moveLeft;
  }

  public override bool CanSpawnAt(TileMap map, float x, float y) {
    for (float i = x - 1f; i < x + 1.5f; i += 1f) {
      if (map.IsSolid(i, y)) return false;
      if (!map.IsSolid(i, y + 1)) return false;
    }
    return true;
  }

  public override void OnKilled() {
    if (GameManager.health < GameManager.maxHealth && UnityEngine.Random.value < .2f) {
      UnityEngine.Object.Instantiate(_network.recoveryPrefab, transform.position, Quaternion.identity);
    }
  }
}
