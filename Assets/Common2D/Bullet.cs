using UnityEngine;

public class Bullet : CharacterCore
{
  public bool friendly = true;
	public bool moveLeft = false;
  public float lifetime;
  public float damage = 1f;
  private float lifetimeRemaining;

  public override void Start() {
    base.Start();
    lifetimeRemaining = lifetime;
    var sam = GetComponent<SpriteAnimationManager>();
    if (sam != null) {
      sam.SwitchAnimation("Idle", true);
    }
  }

	void FixedUpdate() {
    lifetimeRemaining -= Time.deltaTime;
		Vector2 after = Move(new Vector2(moveLeft ? -speed : speed, 0));
		if ((speed != 0 && after.x == 0) || lifetimeRemaining < 0) {
      // yeah, this is an ugly hack, but it's a game jam
      if (gameObject.name.Contains("QShot")) {
        float tx = origin.x + (moveLeft ? -.4f : .4f);
        if (tileMap.TileTypeAt(tx, origin.y - .25f) == 69) {
          tileMap.DestroyAt(tx, origin.y - .25f);
        }
        if (((int)(origin.y - .25f) != (int)(origin.y + .25f)) && tileMap.TileTypeAt(tx, origin.y + .25f) == 69) {
          tileMap.DestroyAt(tx, origin.y + .25f);
        }
      }
			Destroy(this.gameObject);
		}
	}

	public virtual void OnCollisionEnter2D(Collision2D coll)
	{
    CharacterCore victim = null;
    if (friendly) {
      victim = coll.gameObject.GetComponent<EnemyCore>();
    } else {
      victim = coll.gameObject.GetComponent<PlayerController>();
    }
    if (victim == null) {
      return;
    }
    if (friendly) {
      // TODO: this would be better with some sort of BulletRef class
      PlayerController player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
      if (player.activeDrill == this) {
        player.activeDrill = null;
      } else if (player.activeQShot == this) {
        player.activeQShot = null;
      }
    }
    Kill(true);
  }
}
