using UnityEngine;
using System.Collections.Generic;

public class EnemyCore : CharacterCore {
	public float health = 1;
	public int pointValue = 100;
  public float touchDamage = .5f;
  public List<int> foundIn = new List<int>();
  public float blinkTime = 0f;
  public float deathTime = 0f;
  protected SpriteAnimationManager sam;

  public override void Start() {
    base.Start();
    sam = GetComponent<SpriteAnimationManager>();
  }

  public virtual void Update() {
    if (deathTime > 0) {
      deathTime -= Time.deltaTime;
      if (deathTime < 0) {
        Kill(true);
      } else if (deathTime < .25f) {
        blinkTime = deathTime;
      }
    }
    if (blinkTime > 0) {
      blinkTime -= Time.deltaTime;
      if (blinkTime <= 0 || Mathf.Floor(blinkTime * 20f) % 2 == 0) {
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0f);
      } else {
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0.4f);
      }
    }
  }

	public virtual void OnCollisionEnter2D(Collision2D coll)
	{
    var bullet = coll.gameObject.GetComponent<Bullet>();
    if (bullet != null && blinkTime <= 0f && deathTime <= 0f) {
      health -= bullet.damage;
      if (health <= 0) {
        sam.SwitchAnimation("Dead", true);
        sam.PlayOnce("Dying");
        GameManager.score += pointValue;
        blinkTime = .25f;
        deathTime = 1f;
        OnKilled();
      } else {
        sam.PlayOnce("Damaged");
        blinkTime = .5f;
      }
    }
	}

  public virtual bool CanSpawnAt(TileMap map, float x, float y) {
    return !map.IsSolid(x, y);
  }

  public virtual void OnKilled() {
    // no-op
  }
}
