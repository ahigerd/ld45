﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
  Idle,
  Walking,
  InAir,
  Interacting
}

public class PlayerController : CharacterCore
{
  private const float TERMINAL_VELOCITY = 20f;

  public static float armorValue = 1f;

  private float drillSoundTime = 0f;
  public AudioClip jumpSound, thrustSound, fireSound, hurtSound, meleeSound, drillSound, qshotSound, drainSound;
  private AudioSource thrustAudioSource;
  private bool dying = false, isSpawning = false;
  private float deathLerp = 0;
  private bool playedDeathAnimation = false;
  private float deathHue, deathSaturation, deathValue;
  private float debounceTimer = -1f;
  public GameObject bulletPrefab;
  public GameObject meleePrefab;
  public GameObject drillPrefab;
  public GameObject qshotPrefab;
  public Bullet activeDrill = null;
  public Bullet activeQShot = null;
  private List<NetworkTopology.Powerup> powerups = new List<NetworkTopology.Powerup>();
  private int cheatCode = 0;
  public Vector2 respawnPoint = Vector2.zero;

  [HideInInspector]
  public Vector2 velocity = new Vector2(0, 0);

  [HideInInspector]
  public float horizontal_momentum = 0f;

  private float deathTime = -1f;
  private float jumpBuffer = 0f;
  private float jumpStart = 0f;
  private bool jumping = false;
  private bool canBoost = false;
  private bool boosting = false;
  private float boostFuel = 1f;
  private PlayerState playerState = PlayerState.InAir;
  private float blockedTime = 0f;
  private float blinkTime = 0f;

  public int HorizontalDirection
  {
    get
    {
      float raw = Input.GetAxisRaw("Horizontal");
      if (Mathf.Abs(raw) < 0.2) return 0;
      return raw < 0 ? -1 : 1;
    }
  }

  public Vector2 FindStartPoint()
  {
    if (_network == null) {
      _network = GameObject.Find("Map").GetComponent<NetworkTopology>();
    }
    return _network.spawnPoint;
  }

  public override void OnGameStart()
  {
    armorValue = 1f;
    GameManager.lives = 3;
    tileMap.NewLevel();
    playedDeathAnimation = false;
    GameManager.health = GameManager.maxHealth;
    powerups = new List<NetworkTopology.Powerup>();
  }

  public override void OnLevelStart()
  {
    _server = _network.currentServer;
    GameManager.instance.SpawnLife();
  }

  public override void OnPlayerSpawn()
  {
    debounceTimer = Time.time + 0.25f;
    blockedTime = 0;
    deathTime = 0;
    if (dying) {
      GameManager.health = GameManager.maxHealth;
    }
    dying = false;
    deathLerp = 0;
    if (respawnPoint == Vector2.zero) {
      respawnPoint = FindStartPoint();
    }
    origin = respawnPoint;
    velocity = Vector2.zero;
    horizontal_momentum = 0f;
    GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0f);
    GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", 0);
    GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 0);
    gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Idle", true);
    gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Spawn");
    blinkTime = .5f;
    isSpawning = true;
    GameManager.instance.StartLife();
  }

  public override void OnPlayerDeath()
  {
    boosting = false;
    dying = true;
    playedDeathAnimation = false;
    deathTime = 0f;
    audioSource.PlayOneShot(deathSound);
  }

  public override void OnGameOver()
  {
    // so that the first spawn doesn't leave a corpse
    playedDeathAnimation = false;
  }

  public override void Start()
  {
    base.Start();
    origin = FindStartPoint();

    /*
    SpriteRenderer corpsePrefab = Instantiate(((GameObject)Resources.Load("FallenPlayer")).GetComponent<SpriteRenderer>());
    deathHue = corpsePrefab.material.GetFloat("_HueShift");
    deathValue = corpsePrefab.material.GetFloat("_ValueShift");
    deathSaturation = corpsePrefab.material.GetFloat("_SaturationShift");
    Destroy(corpsePrefab);
    */

    GameObject thrustHolder = new GameObject();
    thrustAudioSource = thrustHolder.AddComponent<AudioSource>();
    thrustAudioSource.loop = true;
    thrustAudioSource.clip = thrustSound;
  }

  public void Update()
  {
    if (GameManager.state != GameState.Playing && GameManager.state != GameState.Spawning && GameManager.state != GameState.Dead) {
      return;
    }

    if (!dying && _server.serverID == 1 && !HasPower(NetworkTopology.Powerup.SignalAmplifier)) {
      GameManager.health -= Time.deltaTime * .1f;
      if ((Time.time * 10f) % 1f < .5f) {
        audioSource.PlayOneShot(drainSound);
      }
      if (GameManager.health <= 0) {
        dying = true;
        GameManager.instance.LoseLife();
      }
      GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", -UnityEngine.Random.value);
    } else {
      GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", 0);
    }

    if (blinkTime > 0) {
      blinkTime -= Time.deltaTime;
      if (isSpawning || blinkTime <= 0 || Mathf.Floor(blinkTime * 20f) % 2 == 0) {
        if (blinkTime <= 0) isSpawning = false;
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0f);
      } else {
        GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", 0.4f);
      }
    }

    if (!boosting && thrustAudioSource.volume > 0) {
      thrustAudioSource.volume -= Time.deltaTime * 3f;
      if (thrustAudioSource.volume < 0) {
        thrustAudioSource.Stop();
      }
    }

    if (dying) {
      deathLerp += Time.deltaTime;
      GetComponent<SpriteRenderer>().material.SetFloat("_HueShift", Mathf.Lerp(0f, deathHue, deathLerp));
      GetComponent<SpriteRenderer>().material.SetFloat("_SaturationShift", Mathf.Lerp(0f, deathSaturation, deathLerp));
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", Mathf.Lerp(0f, deathValue, deathLerp));
      if (!IsGrounded()) {
        velocity.y += GRAVITY * Time.deltaTime;
        if (velocity.y > 10) velocity.y = 10;
        velocity = Move(velocity);
      } else {
        if (!playedDeathAnimation) {
          gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Dead", true);
          gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Fail");
          playedDeathAnimation = true;
        }
        deathTime += Time.deltaTime;
        if (deathTime > 1f) {
          if (_server.serverID == 1) {
            _server = _network.currentServer = _network.root;
            GameManager.instance.NewLevel();
          } else {
            GameManager.instance.SpawnLife();
          }
        }
      }
      return;
    }

    if (Time.time > debounceTimer) {
      DoMovement();
      DoAction();
    }

    if (tileMap.IsSolid(midpoint.x, midpoint.y)) {
      blockedTime += Time.deltaTime;
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", -blockedTime);
    } else if (blockedTime > 0) {
      GetComponent<SpriteRenderer>().material.SetFloat("_ValueShift", 0);
      blockedTime = 0f;
    }
    if (blockedTime > 1.0f) {
      GameManager.instance.LoseLife();
    }
  }

  private bool ShouldStartJump()
  {
    if (!IsGrounded()) {
      return false;
    }
    return (Time.time - jumpBuffer) < 0.05;
  }

  private void StartJump()
  {
    audioSource.PlayOneShot(jumpSound);

    jumpStart = Time.time;
    velocity.y = -jumpPower;

    isGrounded = false;
    jumpBuffer = 0f;
    jumping = true;
    playerState = PlayerState.InAir;
  }

  public void DoMovement()
  {
    bool wasGrounded = isGrounded;
    int xAxis = HorizontalDirection;

    if (activeDrill == null) {
      if (Input.GetButtonDown("Jump")) {
        jumpBuffer = Time.time;
      }
      jumping = jumping && Input.GetButton("Jump");
    }

    if (IsGrounded() && playerState != PlayerState.Interacting)
      playerState = PlayerState.Idle;

    switch (playerState) {
      case PlayerState.Walking:
      case PlayerState.Idle:
        DoGrounded();
        break;
      case PlayerState.InAir:
        DoAir();
        break;
    }

    velocity = Move(velocity);

    if (dying) return;

    SpriteAnimationManager sam = gameObject.GetComponent<SpriteAnimationManager>();
    if (!wasGrounded && isGrounded) {
      sam.PlayOnce("Landing");
      boostFuel = 1f;
    } else if (wasGrounded && !isGrounded) {
      sam.PlayOnce("JumpStart");
    } else if (boosting) {
      sam.SwitchAnimation("Boost", true);
    } else if (playerState == PlayerState.Walking) {
      sam.SwitchAnimation("Walking", true);
    } else if (playerState == PlayerState.Idle) {
      sam.SwitchAnimation("Idle", true);
    } else if (activeDrill == null) {
      if (Mathf.Abs(velocity.y) < 1.0) {
        sam.SwitchAnimation("JumpIdle", true);
      } else if (velocity.y < 0) {
        sam.SwitchAnimation("JumpUp", true);
      } else if (velocity.y > 0) {
        sam.SwitchAnimation("JumpDown", true);
      }
    }

    if (playerState == PlayerState.Interacting) return;
    if (Time.time - jumpStart < 0.3) {
      if (Mathf.Abs(velocity.x) > 0.2) {
        GetComponent<SpriteRenderer>().flipX = (velocity.x < 0);
      }
    } else if (xAxis != 0) {
      GetComponent<SpriteRenderer>().flipX = (xAxis < 0);
    }
  }


  private void DoGrounded()
  {
    canBoost = false;
    boosting = false;
    horizontal_momentum = Utility.Approach(horizontal_momentum, (Input.GetAxisRaw("Horizontal") * speed), 0.4f);
    velocity.x = horizontal_momentum;

    if (ShouldStartJump()) {
      StartJump();
    } else {
      velocity.y += GRAVITY * Time.deltaTime;
      if (velocity.y > TERMINAL_VELOCITY) velocity.y = TERMINAL_VELOCITY;
    }

    playerState = (Mathf.Abs(Input.GetAxis("Horizontal")) >= 0.2f && velocity.x != 0) ? PlayerState.Walking : PlayerState.Idle;

    if (!IsGrounded())
      playerState = PlayerState.InAir;
  }


  private void DoAir()
  {
    if (activeDrill != null) {
      velocity.y = 10f;
      return;
    }
    if (canBoost && Input.GetButton("Jump") && HasPower(NetworkTopology.Powerup.Booster) && boostFuel > 0f) {
      boostFuel -= Time.deltaTime;
      if (velocity.y > (jumpPower / -3)) velocity.y = (jumpPower / -3);
      velocity.y -= jumpPower * Time.deltaTime * 1.5f;
      if (velocity.y < TERMINAL_VELOCITY * -1.5f) velocity.y = TERMINAL_VELOCITY * -1.5f;
      if (!boosting) {
        thrustAudioSource.volume = 1f;
        thrustAudioSource.Play();
      }
      boosting = true;
    } else {
      if (boosting) {
        boosting = false;
      }
      if (!jumping && velocity.y < 0) {
        velocity.y += 2.5f * GRAVITY * Time.deltaTime;
      }
      velocity.y += GRAVITY * Time.deltaTime;
      if (velocity.y > TERMINAL_VELOCITY) velocity.y = TERMINAL_VELOCITY;
      if (!jumping) {
        canBoost = true;
      }

      if (ShouldStartJump()) {
        StartJump();
      }
    }

    horizontal_momentum = Utility.Approach(horizontal_momentum, Input.GetAxisRaw("Horizontal") * speed, 0.2f);
    velocity.x = horizontal_momentum;
  }

  private void DoAction()
  {
    if (GameManager.state != GameState.Playing || playerState == PlayerState.Interacting) {
      return;
    }

    if (Input.GetAxisRaw("Vertical") > .5f) {
      if (cheatCode == 0 && Input.GetButtonDown("Fire1")) {
        cheatCode = 1;
      } else if (cheatCode == 1 && Input.GetButtonDown("Jump")) {
        cheatCode = 2;
      } else if (cheatCode == 2 && Input.GetButtonDown("Fire1")) {
        cheatCode = 3;
      } else if (cheatCode == 3 && Input.GetButtonDown("Fire2")) {
        AddPickup(NetworkTopology.Powerup.Melee);
        AddPickup(NetworkTopology.Powerup.Ranged);
        AddPickup(NetworkTopology.Powerup.Charger);
        AddPickup(NetworkTopology.Powerup.Booster);
        AddPickup(NetworkTopology.Powerup.Quarantine);
        AddPickup(NetworkTopology.Powerup.SignalAmplifier);
        AddPickup(NetworkTopology.Powerup.Armor);
        AddPickup(NetworkTopology.Powerup.Health);
      } else if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Fire2") || Input.GetButtonDown("Jump")) {
        cheatCode = 0;
      }
    } else {
      cheatCode = 0;
    }


    bool drillBreakL = tileMap.TileTypeAt(origin.x - .5f, origin.y + .1f) == 68;
    bool drillBreakR = ((int)(origin.x - .5f) != (int)(origin.x + .5f)) && tileMap.TileTypeAt(origin.x + .5f, origin.y + .1f) == 68;
    bool canDrill = activeDrill == null && (playerState == PlayerState.InAir || drillBreakL || drillBreakR);
    if (canDrill && Input.GetButtonDown("Fire1") && Input.GetAxisRaw("Vertical") < -.5f && HasPower(NetworkTopology.Powerup.Charger)) {
      drillSoundTime = 0;
      audioSource.PlayOneShot(drillSound);
      gameObject.GetComponent<SpriteAnimationManager>().SwitchAnimation("Dive", true);
      activeDrill = Instantiate(drillPrefab, transform).GetComponent<Bullet>();
      activeDrill.origin = origin - Vector2.down * .25f;
    } else if (activeDrill != null) {
      if (isGrounded) {
        UnityEngine.Object.Destroy(activeDrill.gameObject);
        activeDrill = null;
        if (drillBreakL) {
          tileMap.DestroyAt(origin.x - .5f, origin.y + .1f);
        }
        if (drillBreakR) {
          tileMap.DestroyAt(origin.x + .5f, origin.y + .1f);
        }
      } else {
        drillSoundTime += Time.deltaTime;
        if (drillSoundTime >= .1f) {
          drillSoundTime = 0;
          audioSource.PlayOneShot(drillSound);
        }
      }
    } else if (Input.GetButtonDown("Fire1") && HasPower(NetworkTopology.Powerup.Melee)) {
      gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Melee");
      audioSource.PlayOneShot(meleeSound);
      bool faceLeft = GetComponent<SpriteRenderer>().flipX;
      Bullet bullet = Instantiate(meleePrefab, transform).GetComponent<Bullet>();
      bullet.origin = origin + new Vector2(faceLeft ? -.4f : .4f, -0f);
      bullet.moveLeft = faceLeft;
      bullet.GetComponent<SpriteRenderer>().flipX = faceLeft;
    } else if (activeQShot == null && Input.GetButtonDown("Fire2") && Input.GetAxisRaw("Vertical") < -.5f && HasPower(NetworkTopology.Powerup.Quarantine)) {
      gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Ranged");
      audioSource.PlayOneShot(qshotSound);
      bool faceLeft = GetComponent<SpriteRenderer>().flipX;
      Bullet bullet = Instantiate(qshotPrefab).GetComponent<Bullet>();
      bullet.origin = origin + new Vector2(faceLeft ? -.4f : .4f, -.65f);
      bullet.moveLeft = faceLeft;
      bullet.GetComponent<SpriteRenderer>().flipX = faceLeft;
      if (faceLeft) {
        var vel = bullet.GetComponentInChildren<ParticleSystem>().velocityOverLifetime;
        var orbitalZ = vel.orbitalZ;
        orbitalZ.constant = -orbitalZ.constant;
        vel.orbitalZ = orbitalZ;
      }
      activeQShot = bullet;
    } else if (Input.GetButtonDown("Fire2") && HasPower(NetworkTopology.Powerup.Ranged)) {
      gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Ranged");
      audioSource.PlayOneShot(fireSound);
      bool faceLeft = GetComponent<SpriteRenderer>().flipX;
      Bullet bullet = Instantiate(bulletPrefab).GetComponent<Bullet>();
      bullet.origin = origin + new Vector2(faceLeft ? -.4f : .4f, -.77f);
      bullet.moveLeft = faceLeft;
      bullet.GetComponent<SpriteRenderer>().flipX = faceLeft;
    }
  }

  public void OnCollisionEnter2D(Collision2D coll)
  {
    if (dying || blinkTime > 0) return;

    var enemy = coll.gameObject.GetComponent<EnemyCore>();
    if (enemy != null && enemy.deathTime <= 0f && enemy.blinkTime <= 0f && enemy.touchDamage > 0f) {
      audioSource.PlayOneShot(hurtSound);
      blinkTime = 1f;
      GameManager.instance.SetHealth(GameManager.health - enemy.touchDamage);
    }
  }

  public override Vector2 Move(Vector2 v)
  {
    Vector2 result = base.Move(v);
    return result;
  }

  public void AddPickup(NetworkTopology.Powerup powerup) {
    if (powerup == NetworkTopology.Powerup.Recovery) {
      GameManager.instance.SetHealth(GameManager.health + .25f);
    } else if (powerup == NetworkTopology.Powerup.Health) {
      GameManager.maxHealth += .5f;
      GameManager.instance.SetHealth(GameManager.health + .5f);
    } else {
      if (powerup == NetworkTopology.Powerup.Armor) {
        armorValue = .5f;
      }
      powerups.Add(powerup);
    }
  }

  public bool HasPower(NetworkTopology.Powerup powerup) {
    return powerups.Contains(powerup);
  }

  public override void OnPlayerDamage() {
    blinkTime = .5f;
    gameObject.GetComponent<SpriteAnimationManager>().PlayOnce("Damaged");
  }
}
