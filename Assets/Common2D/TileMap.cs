using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

//solid=X01~$|
//open=.@*`S^:

public class TileMap : LevelAwareBehaviour {
  private static Dictionary<NetworkTopology.Powerup, Dictionary<byte, byte>> tileConv = new Dictionary<NetworkTopology.Powerup, Dictionary<byte, byte>>() {
    [NetworkTopology.Powerup.None] = new Dictionary<byte, byte>() {
      [(byte)3] = (byte)0,
      [(byte)4] = (byte)0,
      [(byte)5] = (byte)0,
      [(byte)68] = (byte)65,
      [(byte)69] = (byte)65,
      [(byte)70] = (byte)65,
    },
    [NetworkTopology.Powerup.Charger] = new Dictionary<byte, byte>() {
      [(byte)3] = (byte)68,
      [(byte)4] = (byte)65,
      [(byte)5] = (byte)0,
      [(byte)68] = (byte)68,
      [(byte)69] = (byte)65,
      [(byte)70] = (byte)65,
    },
    [NetworkTopology.Powerup.Booster] = new Dictionary<byte, byte>() {
      [(byte)3] = (byte)65,
      [(byte)4] = (byte)65,
      [(byte)5] = (byte)65,
      [(byte)68] = (byte)0,
      [(byte)69] = (byte)0,
      [(byte)70] = (byte)0,
    },
    [NetworkTopology.Powerup.Quarantine] = new Dictionary<byte, byte>() {
      [(byte)3] = (byte)65,
      [(byte)4] = (byte)69,
      [(byte)5] = (byte)0,
      [(byte)68] = (byte)65,
      [(byte)69] = (byte)69,
      [(byte)70] = (byte)65,
    },
    [NetworkTopology.Powerup.SignalAmplifier] = new Dictionary<byte, byte>() {
      [(byte)3] = (byte)0,
      [(byte)4] = (byte)0,
      [(byte)5] = (byte)0,
      [(byte)68] = (byte)65,
      [(byte)69] = (byte)65,
      [(byte)70] = (byte)65,
    },
  };

  public const int MAP_WIDTH = MapBlock.BLOCK_SIZE * 4;
  public const int MAP_HEIGHT = MapBlock.BLOCK_SIZE * 3;
	private const float BACKGROUND_DEPTH = 0.5f;
	private const float TERRAIN_DEPTH = -0.3f;

  public Transform crumblePrefab;
  public Transform freezePrefab;
	public List<EnemyCore> enemyPrefabs = new List<EnemyCore>();
	public List<EnemyCore> collectPrefabs = new List<EnemyCore>();
  private List<EnemyCore> enemies = new List<EnemyCore>();

	public Material material;
	public Texture2D backgroundTexture;
  public bool IsVisible = true;
  [HideInInspector]
  public MapBlock.Placement[,] mapBlocks;
	private Material backgroundMaterial;
	private Mesh backgroundMesh;
  private Mesh mapMesh;
  public NetworkTopology.Server server;

  private byte[,] tiles = new byte[MAP_WIDTH, MAP_HEIGHT];
  private static float tileEpsilon = 0.000001f;
	private static float tileSizeH = 1f / 8f - 2 * tileEpsilon;
	private static float tileSizeV = 1f / 8f - 2 * tileEpsilon;
	private static Vector2 TileToUV(int x, int y) {
		return new Vector2(x * tileSizeH, (7 - y) * tileSizeV);
	}
  // TODO: This will need updated on a per-application basis
	private static Vector2[] tileUV = {
		/* .0. 0*0 .0. */ TileToUV(0, 3),
		/* .0. 0*0 .1. */ TileToUV(0, 3),
		/* .0. 0*1 .0. */ TileToUV(0, 0),
		/* .0. 0*1 .1. */ TileToUV(0, 0),
		/* .0. 1*0 .0. */ TileToUV(2, 0),
		/* .0. 1*0 .1. */ TileToUV(2, 0),
		/* .0. 1*1 .0. */ TileToUV(1, 0),
		/* .0. 1*1 .1. */ TileToUV(1, 0),
		/* .1. 0*0 .0. */ TileToUV(1, 3),
		/* .1. 0*0 .1. */ TileToUV(1, 3),
		/* .1. 0*1 .0. */ TileToUV(0, 2),
		/* .1. 0*1 .1. */ TileToUV(0, 1),
		/* .1. 1*0 .0. */ TileToUV(2, 2),
		/* .1. 1*0 .1. */ TileToUV(2, 1),
		/* .1. 1*1 .0. */ TileToUV(1, 2),
		/* .1. 1*1 .1. */ TileToUV(1, 2),

    TileToUV(0, 4),
    TileToUV(1, 4),
    TileToUV(2, 4),
    TileToUV(1, 4),
  };

	public void GenerateTiles() {
    if (MapBlock.blocks.Count == 0 || mapBlocks == null) {
      return;
    }
    for (int y = 0; y < NetworkTopology.Server.BLOCKS_Y; y += 1) {
      int by = y * MapBlock.BLOCK_SIZE;
      for (int x = 0; x < NetworkTopology.Server.BLOCKS_X; x += 1) {
        MapBlock.Placement block = mapBlocks[x, y];
        int bx = x * MapBlock.BLOCK_SIZE;
        for (int sy = 0; sy < MapBlock.BLOCK_SIZE; sy++) {
          for (int sx = 0; sx < MapBlock.BLOCK_SIZE; sx++) {
            if (block == null) {
              tiles[bx + sx, by + sy] = (byte)0;
              continue;
            }
            byte tile = block.tiles[sx, sy];
            if (!tileConv.ContainsKey(block.powerupRequired)) {
              Debug.Log(block.powerupRequired);
            }
            if (block.item == NetworkTopology.Powerup.None && tile >= 2 && tile <= 6) {
              tile = (byte)65;
            } else if (block.item == NetworkTopology.Powerup.None && tile == 71) {
              tile = (byte)0;
            } else if (tile == 1 || tile == 2 || tile == 6) {
              tile = (byte)0;
            } else if (tileConv[block.powerupRequired].ContainsKey(tile)) {
              if (block.item == NetworkTopology.Powerup.None) {
                tile = (byte)65;
              } else {
                tile = tileConv[block.powerupRequired][tile];
              }
            }
            tiles[bx + sx, by + sy] = tile;
          }
        }
      }
    }
		PopulateMesh();
	}

	public void Awake()
	{
		backgroundMaterial = new Material(material);
		backgroundMaterial.mainTexture = backgroundTexture;
		backgroundMesh = new Mesh();
		backgroundMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(new Vector3(0, 0, 0));
		vertices.Add(new Vector3(MAP_WIDTH, 0, 0));
		vertices.Add(new Vector3(MAP_WIDTH, -MAP_HEIGHT, 0));
		vertices.Add(new Vector3(0, -MAP_HEIGHT, 0));
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));
		uv.Add(new Vector2(0, 1));
		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(2);
		triangles.Add(3);
		triangles.Add(0);
		backgroundMesh.vertices = vertices.ToArray();
		backgroundMesh.uv = uv.ToArray();
		backgroundMesh.triangles = triangles.ToArray();

    GenerateTiles();
		PopulateMesh();
	}

	public override void OnGameStart() {
		foreach (EnemyCore enemy in enemies) {
			Destroy(enemy.gameObject);
		}
		enemies.Clear();
	}

	public override void OnPlayerSpawn() {
	}

	public void NewLevel() {
    GenerateTiles();
    PopulateMesh();
	}

	public byte TileTypeAt(float x, float y)
	{
		if (x < 0 || y < 0 || x >= MAP_WIDTH || y >= MAP_HEIGHT) {
			return (byte)65;
		}

		return tiles[(int)x, (int)y];
	}

	public bool IsSolid(float x, float y)
	{
		byte tileType = TileTypeAt(x, y);
		return tileType > 64;
	}

	public void PopulateMesh()
	{
    if (mapMesh == null) {
      mapMesh = new Mesh();
    }
		mapMesh.Clear();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		int triangleBase = 0;
		for (int y = 0; y < MAP_HEIGHT; y++) {
			for (int x = 0; x < MAP_WIDTH; x++) {
        byte tile = TileTypeAt(x, y);
        if (tile == 6) {
        } else if (tile >= 3 && tile < 6) {
          tile = (byte)(62 + tile);
        } else if (tile <= 64) {
          continue;
        }
				vertices.Add(new Vector3(x, -y, 0));
				vertices.Add(new Vector3(x + 1, -y, 0));
				vertices.Add(new Vector3(x + 1, -y - 1, 0));
				vertices.Add(new Vector3(x, -y - 1, 0));
				int bits = 0;
        if (tile > 65 && tile <= 69) {
          bits = tile - 50;
        } else {
          if (IsSolid(x, y-1)) bits |= 8;
          if (IsSolid(x-1, y)) bits |= 4;
          if (IsSolid(x+1, y)) bits |= 2;
          if (IsSolid(x, y+1)) bits |= 1;
        }
				Vector2 uvPos = tileUV[bits];
				uv.Add(new Vector2(uvPos.x + tileEpsilon, uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH,   uvPos.y + tileSizeV));
				uv.Add(new Vector2(uvPos.x + tileSizeH,   uvPos.y + tileEpsilon));
				uv.Add(new Vector2(uvPos.x + tileEpsilon, uvPos.y + tileEpsilon));
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+1);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase);
				triangles.Add(triangleBase+2);
				triangles.Add(triangleBase+3);
				triangleBase += 4;
			}
		}
		mapMesh.vertices = vertices.ToArray();
		mapMesh.uv = uv.ToArray();
		mapMesh.triangles = triangles.ToArray();
	}

  public List<Vector2Int> destroyChain = new List<Vector2Int>();
  public List<Vector2Int> lastChain = new List<Vector2Int>();
  private float destroyTimer = 0;
  public void DestroyAt(float x, float y) {
    Vector2Int v = new Vector2Int((int)x, (int)y);
    if (tiles[v.x, v.y] != 0 && !destroyChain.Contains(v) && !lastChain.Contains(v)) {
      destroyChain.Add(v);
    }
  }

	public void Update() {
    if (destroyChain.Count > 0) {
      if (destroyTimer <= 0) {
        destroyTimer = .24f;
        lastChain = destroyChain.GetRange(0, destroyChain.Count);
        destroyChain.Clear();
        AudioClip clip = null;
        AudioSource source = null;
        foreach (Vector2Int v in lastChain) {
          Bullet bullet = null;
          if (tiles[v.x, v.y] == 68) {
            bullet = UnityEngine.Object.Instantiate(crumblePrefab, new Vector3(v.x, -v.y, 0f), Quaternion.identity, transform).GetComponent<Bullet>();
          } else if (tiles[v.x, v.y] == 69) {
            bullet = UnityEngine.Object.Instantiate(freezePrefab, new Vector3(v.x, -v.y, 0f), Quaternion.identity, transform).GetComponent<Bullet>();
          } else {
            continue;
          }
          if (bullet != null && clip == null) {
            clip = bullet.deathSound;
          }
          if (tiles[v.x - 1, v.y] == tiles[v.x, v.y]) DestroyAt(v.x - 1, v.y);
          if (tiles[v.x + 1, v.y] == tiles[v.x, v.y]) DestroyAt(v.x + 1, v.y);
          if (tiles[v.x, v.y - 1] == tiles[v.x, v.y]) DestroyAt(v.x, v.y - 1);
          if (tiles[v.x, v.y + 1] == tiles[v.x, v.y]) DestroyAt(v.x, v.y + 1);
          tiles[v.x, v.y] = 0;
        }
        lastChain.Clear();
        PopulateMesh();
        GameObject.FindWithTag("Player").GetComponent<PlayerController>().audioSource.PlayOneShot(clip);
      } else {
        destroyTimer -= Time.deltaTime;
      }
    }
    if (IsVisible) {
      Graphics.DrawMesh(backgroundMesh, new Vector3(0, 0, TileMap.BACKGROUND_DEPTH), Quaternion.identity, backgroundMaterial, 0, null, 0, null, false, false, false);
      Graphics.DrawMesh(mapMesh, new Vector3(0, 0, TileMap.TERRAIN_DEPTH), Quaternion.identity, material, 0, null, 0, null, false, false, false);
    }
	}
}
