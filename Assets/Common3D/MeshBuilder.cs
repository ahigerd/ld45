using UnityEngine;
using System.Collections.Generic;

class MeshBuilder {
  public List<Vector3> vertices = new List<Vector3>();
  public List<Vector2> uv = new List<Vector2>();
  public List<int> triangles = new List<int>();
  public Dictionary<string, int> cache = new Dictionary<string, int>();

  public Mesh GetMesh() {
    Mesh mesh = new Mesh();
    mesh.vertices = vertices.ToArray();
    mesh.uv = uv.ToArray();
    mesh.triangles = triangles.ToArray();
    return mesh;
  }

  public int Get(float x, float y, float z, float u, float v) {
    return Get("", x, y, z, u, v);
  }

  public int Get(string tag, float x, float y, float z, float u, float v) {
    Vector3 vec = new Vector3(x, y, z);
    string key = tag + vec.ToString();
    if (cache.ContainsKey(key)) {
      return cache[key];
    }
    int index = vertices.Count;
    cache.Add(key, index);
    vertices.Add(vec);
    uv.Add(new Vector2(u, v));
    return index;
  }

  public void XZQuad(float xMin, float zMin, float xMax, float zMax, float y, bool flipU, bool flipV) {
    int v1 = Get("xz", xMin, y, zMin, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("xz", xMax, y, zMin, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("xz", xMin, y, zMax, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("xz", xMax, y, zMax, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void XYQuad(float xMin, float yMin, float xMax, float yMax, float z, bool flipU, bool flipV) {
    int v1 = Get("xy", xMin, yMin, z, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("xy", xMax, yMin, z, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("xy", xMin, yMax, z, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("xy", xMax, yMax, z, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void YZQuad(float yMin, float zMin, float yMax, float zMax, float x, bool flipU, bool flipV) {
    int v1 = Get("yz", x, yMin, zMin, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get("yz", x, yMax, zMin, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get("yz", x, yMin, zMax, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get("yz", x, yMax, zMax, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }

  public void Quad(string key, Vector3 a, Vector3 b, Vector3 c, Vector3 d, bool flipU, bool flipV) {
    int v1 = Get(key, a.x, a.y, a.z, flipU ? 1 : 0, flipV ? 1 : 0);
    int v2 = Get(key, b.x, b.y, b.z, flipU ? 0 : 1, flipV ? 1 : 0);
    int v3 = Get(key, c.x, c.y, c.z, flipU ? 1 : 0, flipV ? 0 : 1);
    int v4 = Get(key, d.x, d.y, d.z, flipU ? 0 : 1, flipV ? 0 : 1);
    triangles.Add(v1);
    triangles.Add(v3);
    triangles.Add(v2);
    triangles.Add(v3);
    triangles.Add(v4);
    triangles.Add(v2);
  }
}


