using UnityEngine;

class PointText : MonoBehaviour {
	private float lifetime = 2f;

	public string Text {
		get {
			return GetComponent<TextMesh>().text;
		}
		set {
			GetComponent<TextMesh>().text = value;
		}
	}

	void Update() {
		lifetime -= Time.deltaTime;
		if (lifetime < 0) {
			Object.Destroy(gameObject);
		} else {
			transform.position += Vector3.up * Time.deltaTime * 20f;
		}
	}
}
